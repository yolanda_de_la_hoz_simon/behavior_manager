#include "../include/behavior_manager.h"
#include <iostream>

using namespace std;

// Falta: deactivate behaviors, check consistency.                                                                                                                               <------------------------------------Borrar
BehaviorManager::BehaviorManager(int argc, char **argv):DroneProcess(argc, argv)
{
  printf("%s\n", "Starting behavior_manager_process");

  //Get params
  if (!n.getParam("requested_action", requested_action))
    requested_action = "requested_action";

  if (!n.getParam("droneMissionHLCommandAck", droneMissionHLCommandAck))
    droneMissionHLCommandAck = "droneMissionHLCommandAck";

  if (!n.getParam("droneManagerStatus", droneManagerStatus))
    droneManagerStatus = "droneManagerStatus";
  
  if (!n.getParam("approved_action", approved_action))
    approved_action = "approved_action";

  if (!n.getParam("behaviors_list", behaviors_list_topic))
    behaviors_list_topic = "behaviors_list";

  if (!n.getParam("droneMissionPlannerCommand", droneMissionPlannerCommand))
    droneMissionPlannerCommand = "droneMissionPlannerCommand";

  if (!n.getParam("dronePositionRefs", dronePositionRefs))
    dronePositionRefs = "dronePositionRefs";

  if (!n.getParam("droneSpeedsRefs", droneSpeedsRefs))
    droneSpeedsRefs = "droneSpeedsRefs";

  if (!n.getParam("pointToLook", pointToLook))
    pointToLook = "pointToLook";

  if (!n.getParam("yawToLook", yawToLook))
    yawToLook = "yawToLook";
 
}

BehaviorManager::~BehaviorManager()
{
  printf("%s\n", "Closing behavior_manager_process");
}

void BehaviorManager::ownOpen()
{
  printf("%s\n", "Initializing behavior_manager_process");

  // set services
  behavior_request_srv=n.advertiseService("request_behavior",&BehaviorManager::behaviorRequestCall,this);

  // set subscribers
  action_request_sub = n.subscribe(requested_action, 1000, &BehaviorManager::actionRequestCallback, this);
  command_ack_sub = n.subscribe(droneMissionHLCommandAck, 1000, &BehaviorManager::commandAckCallback, this);
  drone_manager_status_sub = n.subscribe(droneManagerStatus, 1000, &BehaviorManager::droneManagerStatusCallback, this);

  // set publishers
  approved_action_pub = n.advertise<droneMsgsROS::ApprovedAction>(approved_action, 10);
  behaviors_list_pub = n.advertise<droneMsgsROS::BehaviorsList>(behaviors_list_topic, 10);
  command_pub = n.advertise<droneMsgsROS::droneMissionPlannerCommand>(droneMissionPlannerCommand, 10);
  position_refs_pub = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>(dronePositionRefs, 10);
  //droneSpeedsRefsPub = n.advertise<droneMsgsROS::droneSpeedsRefCommand>(droneSpeedsRefs, 10);
  point_to_look_pub = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>(pointToLook, 10);
  yaw_to_look_pub = n.advertise<droneMsgsROS::droneYawRefCommand>(yawToLook, 10);

  readXMLConfig();


}

void BehaviorManager::ownRun()
{
  printf("%s\n", "Starting behavior_manager_process ownRun");
  ros::Rate r(2);
  while(ros::ok())
  {
    ros::spinOnce();
    sendBehaviorsList();
  }
   DroneProcess::sleep();
}

void BehaviorManager::ownRecover()
{
  printf("%s\n", "Starting Behavior Manager ownRecover");
}

void BehaviorManager::printModulesNames(std::vector<std::string> drone_modules)
{
  std::cout << "----LIST OF MODULES TO BE SEND----" << std::endl;
  std::vector<string>::iterator iterador = drone_modules.begin();
  while(iterador != drone_modules.end())
  {
    std::cout << *iterador << std::endl;
    iterador++;
  }
}


bool BehaviorManager::getBehavior(std::string name, behavior** current_behavior)
{
  std::vector<behavior>::iterator iterador = behaviors_list.begin();
  while(iterador != behaviors_list.end())
  {
    if(name.compare(iterador->behavior_name)==0)
    {
      *current_behavior = &(*iterador);
      return true;
    }
    iterador++;
  }
  return false;
}

std::vector<std::string> BehaviorManager::translateBehaviorsIntoProcess()
{
  std::vector<std::string> processes_to_activate;
  std::vector<behavior>::iterator iterador = behaviors_list.begin();
  while(iterador != behaviors_list.end())
  {
    if(iterador->current_state==1)// requested state
    {
      processes_to_activate.insert(std::end(processes_to_activate), std::begin(iterador->process_list), std::end(iterador->process_list));
    }
    iterador++;
  }
  return processes_to_activate;
}

// send the behavior list to the HMI
void BehaviorManager::sendBehaviorsList()
{
    droneMsgsROS::BehaviorsList behavior_list_msg;
    droneMsgsROS::BehaviorDescriptor behavior_descriptor;
    for (int idx = 0; idx < behaviors_list.size(); idx++)
     {
       behavior_descriptor.name = behaviors_list[idx].behavior_name;
       behavior_descriptor.current_state.state = (int) behaviors_list[idx].current_state;
       behavior_list_msg.behavior_list.push_back(behavior_descriptor);
     }
     behaviors_list_pub.publish(behavior_list_msg);
}

std::vector<std::string> BehaviorManager::translateActionIntoProcesses(std::string name)
{
  std::vector<std::string> processes_to_activate;
  std::vector<action>::iterator iterador = actions_list.begin();
  while(iterador != actions_list.end())
  {
    if(name.compare(iterador->action_name)==0)
    processes_to_activate.insert(std::end(processes_to_activate), std::begin(iterador->process_list), std::end(iterador->process_list));

    iterador++;
  }
  return processes_to_activate;
}

void BehaviorManager::initActionsList()
{
    cout << "\033[1;33m............INIT ACTIONS............\033[0m" << endl;
    for (pugi::xml_node root = doc.child("action"); root; root = root.next_sibling("action"))
    {
        std::cout << "\033[1;34m...Action: \033[0m" << root.attribute("name").as_string() << std::endl;
        action new_action;
        new_action.action_name=root.attribute("name").as_string();

        for (pugi::xml_node tool = root.child("process"); tool; tool = tool.next_sibling("process"))
        {
           std::cout << "\033[1;34m........Process: \033[0m" << "\033[1;32m" << tool.attribute("name").as_string() <<"\033[0m"<< std::endl;
           new_action.process_list.push_back(tool.attribute("name").as_string());
         }
         actions_list.push_back(new_action);
    }
    return;
}

void BehaviorManager::initBehaviorsList()
{
    cout << "\033[1;33m............INIT BEHAVIORS............\033[0m" << endl;
    for (pugi::xml_node root = doc.child("behavior"); root; root = root.next_sibling("behavior"))
    {
        std::cout << "\033[1;34m...Behavior: \033[0m" << root.attribute("name").as_string() << std::endl;
        behavior new_behavior;
        new_behavior.behavior_name=root.attribute("name").as_string();
        new_behavior.current_state=State::NotRequested;

        for (pugi::xml_node tool = root.child("process"); tool; tool = tool.next_sibling("process"))
        {
           std::cout << "\033[1;34m........Process: \033[0m" << "\033[1;32m" << tool.attribute("name").as_string() <<"\033[0m"<< std::endl;
           new_behavior.process_list.push_back(tool.attribute("name").as_string());
         }
         behaviors_list.push_back(new_behavior);
    }
    return;
}

bool BehaviorManager::parseXMLConfig(std::string modelOfBehaviorsConfigFile)
{
    // xml parser
    std::ifstream nameFile(modelOfBehaviorsConfigFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result){
       std::cout << "XML [" << modelOfBehaviorsConfigFile << "] \n";
       std::cout << "Error description: " << result.description() << "\n";
       std::cout << "Error offset: " << result.offset << " (error at [..." << (modelOfBehaviorsConfigFile.c_str() + result.offset) << "]\n\n";
       return false;
     }else
    return true;
}

void BehaviorManager::readXMLConfig()
{
    // Parameters config file
    ros::param::get("~model_of_behaviors", xmlConfig);
    ros::param::get("~my_stack_directory", my_stack_directory);
    if (xmlConfig.length() == 0)
    {
        xmlConfig="modelOfBehaviors.xml";
    }

    if (my_stack_directory.length() == 0)
    {
        my_stack_directory="/home/yolanda/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack";
    }

    if(parseXMLConfig(my_stack_directory + "/stack/behavior_manager/" + xmlConfig)){ // TODO:change this to get the file propertly
       initBehaviorsList();
       initActionsList();
    }


    return;
}

bool BehaviorManager::behaviorRequestCall(droneMsgsROS::behaviorRequest::Request &request, droneMsgsROS::behaviorRequest::Response &response)
{
  behavior* current_behavior;
  if(request.activate)
   {
     if(checkConsistency()){
       std::cout << "Saving request behavior in behaviors_list: " + request.behavior_name << std::endl;
       if (getBehavior(request.behavior_name.c_str(),&current_behavior)){
           current_behavior->current_state=State::Requested;
           response.ack = true;
       }else{
         response.ack = false;
         response.why = "Incorrect behavior";
       }
     }else{ 
       response.ack = false;
       response.why = "Inconsistent behavior";
     }
   }else{
     std::cout << "Erasing behavior from behaviors_list..." << std::endl;
     if(!behaviors_list.empty()){
      // found ?
       //
      response.ack = true;
      // not found ?
      response.ack = false;
      response.why = "Incorrect behavior";
      }
   }
  sendBehaviorsList(); // send the behavior list to the HMI
  return true;
}

void BehaviorManager::actionRequestCallback(const droneMsgsROS::RequestedAction::ConstPtr& msg)
{
  int32_t last_action_request = msg->mpAction;
  std::string action;

  droneMsgsROS::droneMissionPlannerCommand drone_command_msg;

  switch(last_action_request)
  {
    case droneMsgsROS::RequestedAction::TAKE_OFF:
      std::cout << "Received Take-Off action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
      action="TAKE_OFF";
      break;
    case droneMsgsROS::RequestedAction::HOVER:
      std::cout << "Received Hover action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::HOVER;
      action="HOVER";
      break;
    case droneMsgsROS::RequestedAction::STABILIZE:
      std::cout << "Received Stabilize action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::HOVER;
      action="MOVE_POSITION";
      break;
    case droneMsgsROS::RequestedAction::MOVE:
      std::cout << "Received Move action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
      action="MOVE_POSITION";
      break;
    case droneMsgsROS::RequestedAction::FLIP_RIGHT:
      std::cout << "Received Flip-Right action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
      action="MOVE_FLIP_RIGHT";
      break;
    case droneMsgsROS::RequestedAction::FLIP_LEFT:
      std::cout << "Received Flip-Left action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
      action="MOVE_FLIP_LEFT";
      break;
    case droneMsgsROS::RequestedAction::FLIP_FRONT:
      std::cout << "Received Flip-Front action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
      action="mpAction";
      break;
    case droneMsgsROS::RequestedAction::FLIP_BACK:
      std::cout << "Received Flip-Back action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
      action="MOVE_FLIP_BACK";
      break;
    case droneMsgsROS::RequestedAction::LAND:
      std::cout << "Received Land action request" << std::endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::LAND;
      action="LAND";
      break;
    default:
      printf("%s\n", "bad request");
      break;
  }

  // get the processes associated to the action and append the drone_modules
  std::cout << "translating action into modules" << std::endl;
  std::vector<std::string> drone_modules; // It stores the modules to be sent to the ManagerOfActions.
  drone_modules=translateActionIntoProcesses(action);

  // get the processes associated to the behavior and append the drone_modules
  std::cout << "translating behaviors into modules" << std::endl;
  std::vector<std::string> behavior_modules = translateBehaviorsIntoProcess();
  drone_modules.insert(std::end(drone_modules), std::begin(behavior_modules), std::end(behavior_modules));

  printModulesNames(drone_modules);
  drone_command_msg.drone_modules_names =drone_modules;
  command_pub.publish(drone_command_msg);
}

// TODO: PREGUNTAR A MARTIN... ACCION PARA QUE LA INTERFAZ FUNCIONE (ANTIGUO MOVE_POSITION) Y BORRAR DE LA INTERFAZ                                                                                                                                          <------------------------------------Borrar
void BehaviorManager::sendCommandInPositionControlMode(double controller_step_command_x, double controller_step_command_y, double controller_step_command_z)
{
    droneMsgsROS::droneMissionPlannerCommand drone_command_msg;
    std::cout<<"Behavior manager  comand move() IN POSITION sent"<<std::endl;

    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
    drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
    drone_command_msg.drone_modules_names.clear();
    command_pub.publish(drone_command_msg);


    if(controller_step_command_x !=0 || controller_step_command_y != 0 || controller_step_command_z != 0)
    {
        double current_xs, current_ys, current_zs;
        current_xs = current_drone_position_reference.x;
        current_ys = current_drone_position_reference.y;
        current_zs = current_drone_position_reference.z;
        std::cout<<"Position reference to be send: " <<std::endl;
        std::cout<<"current pos.x: "+ boost::lexical_cast<std::string>(current_drone_position_reference.x) + " current pos.y: " + boost::lexical_cast<std::string>(current_drone_position_reference.y) + " current pos.z: "+ boost::lexical_cast<std::string>(current_drone_position_reference.z)  <<std::endl;


        drone_position_reference.header.stamp  = ros::Time::now();
        drone_position_reference.position_command.x = current_xs + controller_step_command_x;
        drone_position_reference.position_command.y = current_ys + controller_step_command_y;
        drone_position_reference.position_command.z = current_zs + controller_step_command_z;

        position_refs_pub.publish(drone_position_reference);
    }
}

// TODO                                                                                                                                                                                                                                                       <------------------------------------Borrar
bool BehaviorManager::checkConsistency()
{

  return true;
}

void BehaviorManager::commandAckCallback(const droneMsgsROS::droneHLCommandAck &msg)
{
   std::cout << "Sending approved_action..." << std::endl;
  droneMsgsROS::ApprovedAction approved_action_msg;
  approved_action_msg.ack = msg.ack;
  // publish approved action 
  approved_action_pub.publish(approved_action_msg);
}

void BehaviorManager::droneManagerStatusCallback(const droneMsgsROS::droneManagerStatus &msg)
{
  printf("%s\n", "Starting droneManagerStatus topic callback");

  if(action_ack)
  {
    //currentActionPub.publish(msg);
   // droneMsgsROS::ActiveBehaviors activeBehavior;
    // TODO: Publish behavior list
    // activeBehavior = activeBehaviorList
    //activeBehaviorsPub.publish(activeBehavior);
  }
}

// This main follow the normal pattern to execute a DroneProcess class
int main(int argc, char **argv)
{
  BehaviorManager behavior_manager_process(argc, argv);
  behavior_manager_process.open();
  behavior_manager_process.start();
  return 0;
}
