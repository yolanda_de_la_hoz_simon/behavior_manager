/*!*******************************************************************************************
 *  \file       behavior_manager.h
 *  \brief      BehaviorManager definition file.
 *  \details    This file includes the BehaviorManager class declaration. To obtain more
 *              information about it's definition consult the behavior_manager.cpp file.
 *  \author     Oscar Fernández, Yolanda de la Hoz
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef BEHAVIOR_MANAGER
#define BEHAVIOR_MANAGER

#include "drone_process.h"
#include "std_msgs/String.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/ApprovedAction.h"
#include "droneMsgsROS/BehaviorDescriptor.h"
#include "droneMsgsROS/BehaviorsList.h"
#include "droneMsgsROS/BehaviorState.h"
#include "droneMsgsROS/behaviorRequest.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/RequestedAction.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
//#include "droneMsgsROS/droneSpeedsRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "dronemoduleinterface.h"
#include "droneModuleInterfaceList.h"

//ifstream
#include <fstream>
//XML parser
#include "pugixml.hpp"

/*!******************************************************************************************
 *  \class      BehaviorManager
 *  \brief      The BehaviorManager is the node responsible of translate requested behaviors and actions.
 *  \details    The BehaviorManager has the following main functions:
 *
 *               Translate requested actions (e.g., take off, move to a point, etc.) into specific actions for the motor system, i.e., orders for the controllers.
 *
 *               Translate desired active behaviors (e.g., interpret Aruco visual markers) into running processes.
 *
 *               Guarantee the consistency of requested actions and behaviors.
 *
 *********************************************************************************************/
class BehaviorManager : public DroneProcess
{
public:
  //! Constructor. \details The BehaviorManager constructor doesn't need any argument through argv.
  BehaviorManager(int argc, char **argv);
  ~BehaviorManager();
  void readXMLConfig();


  virtual void ownOpen();
  virtual void ownRun();
  virtual void ownRecover();


private:
  ros::NodeHandle n;

  pugi::xml_document doc;                                              //!< Attribute storing the reference to the document

   typedef enum
  {
    NotRequested,
    Requested
  } State;

   /*!***************************************************************************************
   * \struct behavior
   * \brief  This struct store the list of behaviors (name, state and associated processes)
   ****************************************************************************************/
   struct behavior
  {
    std::string behavior_name;                                         //!< Name of the behavior
    State current_state;                                               //!< Current state of the behavior
    std::vector<std::string> process_list;                             //!< Required processes to activate the behavior
  };

   /*!***************************************************************************************
   * \struct action
   * \brief  This struct store the list of actions (name and associated processes)
   ****************************************************************************************/
   struct action
  {
    std::string action_name;                                           //!< Name of the action
    std::vector<std::string> process_list;                             //!< Required processes to activate the action
  };

  droneMsgsROS::dronePose   current_drone_position_reference;
  droneMsgsROS::droneSpeeds current_drone_speed_reference;


  std::string requested_action;                                        //!< Attribute storing topic name to request action from MissionPlannerScheduler and HMI.
  std::string droneMissionHLCommandAck;                                //!< Attribute storing topic name to receive the acknowledge from ManagerOfActions.
  std::string droneManagerStatus;                                      //!< Attribute storing topic name to receive the current state from ManagerOfActions.
  std::string approved_action;                                         //!< Attribute storing topic name to publish the action that has been approved and send to the ManagerOfActions.
  std::string behaviors_list_topic;                                    //!< Attribute storing topic name to publish the requested behaviors.
  std::string droneMissionPlannerCommand;                              //!< Attribute storing topic name to publish commands to the ManagerOfActions.
  std::string dronePositionRefs;                                       //!< Attribute storing topic name to publish position reference commands to the motor system.
  std::string droneSpeedsRefs;                                         //!< Attribute storing topic name to publish speed reference commands to the motor system.
  std::string pointToLook;                                             //!< Attribute storing topic name to publish the point to look to the yawCommander
  std::string yawToLook;                                               //!< Attribute storing topic name to publish the yaw to look to the yawCommander

  bool action_ack;                                                     //!< Attribute storing topic name to receive alive messages from DroneProcess.

  std::vector<behavior> behaviors_list;                                //!< Attribute storing the behavior list
  std::vector<action> actions_list;                                    //!< Attribute storing the action list


  ros::ServiceServer behavior_request_srv;                             //!< ROS service handler used to request behaviors.

  ros::Subscriber action_request_sub;                                  //!< ROS subscriber handler used to receives the request action from the HMI or MissionPlannerScheduler.
  ros::Subscriber command_ack_sub;                                     //!< ROS subscriber handler used to receives the acknowledge from the ManagerOfActions.
  ros::Subscriber drone_manager_status_sub;                            //!< ROS subscriber handler used to receives the current state of the ManagerOfActions.

  ros::Publisher approved_action_pub;                                  //!< ROS publisher handler used to send the approved action to the HMI and actionMonitor.
  ros::Publisher behaviors_list_pub;                                   //!< ROS publisher handler used to send the state of the behaviors to the HMI.
  ros::Publisher command_pub;                                          //!< ROS publisher handler used to send commands to the ManagerOfActions.
  ros::Publisher position_refs_pub;                                    //!< ROS publisher handler used to send position reference commands to the motor system.
  ros::Publisher speeds_refs_pub;                                      //!< ROS publisher handler used to send speed reference commands to the motor system.
  ros::Publisher point_to_look_pub;                                    //!< ROS publisher handler used to send the point to look to the yawCommander.
  ros::Publisher yaw_to_look_pub;                                      //!< ROS publisher handler used to send the yaw to look to the yawCommander.

  bool parseXMLConfig(std::string modelOfBehaviorsConfigFile);


  void sendBehaviorsList();
  bool getBehavior(std::string name, behavior** current_behavior);
  void initBehaviorsList();
  bool getAction(std::string name, action** current_action);
  void initActionsList();
  std::vector<std::string> translateBehaviorsIntoProcess();
  std::vector<std::string> translateActionIntoProcesses(std::string name);
  void printModulesNames(std::vector<std::string> drone_modules);

  /*!***************************************************************************************
   * \details Check the consistency of the requested behavior
   * \param behavior_name The behavior name
   * \return bool function
   ****************************************************************************************/
  bool checkConsistency();
  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *  'requested_action' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void actionRequestCallback(const droneMsgsROS::RequestedAction::ConstPtr& msg);

   /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *  'droneMissionHLCommandAck' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void commandAckCallback(const droneMsgsROS::droneHLCommandAck &msg);

   /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the
   *  'droneManagerStatus' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void droneManagerStatusCallback(const droneMsgsROS::droneManagerStatus &msg);


  /*!***************************************************************************************
   * \details Service for request behaviors
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/
  bool behaviorRequestCall(droneMsgsROS::behaviorRequest::Request &request, droneMsgsROS::behaviorRequest::Response &response);


  void sendCommandInPositionControlMode(double controller_step_command_x, double controller_step_command_y, double controller_step_command_z);


protected:
  std::string xmlConfig;
  std::string my_stack_directory;

};

#endif
